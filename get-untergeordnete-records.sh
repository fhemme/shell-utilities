#!/bin/bash

# SRU.SH -- Programm zur Abfrage der SRU-Schnittstelle
#
# Autor: Felix Hemme
# Version: v2, 2022-08-17
#
#================
# Funktionsweise:
#================
# Aufruf über
# ./get-untergeordnete-records.sh [ppn] [target]
#
#================
# Testfall:
#================
# ppn der Ueberordnung 1679102346

# Variablen
ppn=${1}
defaultTarget="k10plus"
target="${2:-$defaultTarget}"
format="picaxml"

display_usage() {
    echo -e "\nDas Programm erwartet zwei Argumente beim Aufruf:\n./get-untergeordnete-records.sh [ppn] [Target]\\n
Sofern kein Target angegeben wird, werden die Daten aus dem K10plus abgezogen.\n"
}

display_targets() {
	echo -e "Folgende Targets sind verfügbar: https://wiki.k10plus.de/display/K10PLUS/Datenbanken\n"
}

if [[ $# -eq 0 ]] ; then
    echo -e "\nFEHLER: Keine PPN angegeben."
    display_usage
	display_targets
    exit 1
fi

# Ergebnisse des vorherigen Programms loeschen
rm -f records-filtered.xml

# Download der Ueberordnung und den verknuepften Unterordnungen
# Ergebnisse werden in Datei 'records.xml' geschrieben
curl "https://sru.k10plus.de/${target}!levels=0?version=1.1&operation=searchRetrieve&query=pica.1049%3D${ppn}+and+pica.1045%3Drel-nt+and+pica.1001%3Db&maximumRecords=100&recordSchema=${format}" > records.xml
#curl "https://sru.bsz-bw.de/swb299!levels=0?version=1.1&operation=searchRetrieve&query=pica.1049%3D$1+and+pica.1045%3Drel-nt+and+pica.1001%3Db&maximumRecords=100&recordSchema=picaxml" > records.xml

# Extraktion der ppns 
# !! Regex muss bei anderem Format, z. B. marcxml, angepasst werden !!
# IDs werden in Datei 'ppns.txt' geschrieben
grep -A 2 "<datafield tag=\"003@\">" records.xml | grep -Eo "[0-9X]{9,10}" > ppns.txt

# Nur Unterordnungen anfragen unter Verwendung der ppns der vorherigen Anfragen
# Ergebnisse werden in Datei 'records-filtered.xml' geschrieben
while IFS="" read -r p || [ -n "$p" ]
do
  printf '%s\n' "$p"
  # ueber SRU
  #curl "https://sru.bsz-bw.de/swbtest?version=1.1&query=pica.ppn%3D${p}+and+pica.bbg%3D%28%22Os*%22+or+pica.bbg%3D%22As*%22%29&operation=searchRetrieve&maximumRecords=10&recordSchema=picaxml" >> records-filtered.xml
  # ueber unAPI
  curl "https://unapi.k10plus.de/?id=${target}:ppn:${p}&format=${format}" >> records-filtered.xml
done < ppns.txt

# Aufraeumen
rm records.xml
rm ppns.txt
